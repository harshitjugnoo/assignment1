/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.Brand;
import com.app.entities.Cars;
/**
 *
 * @author Harshit
 */
public class Maruti extends Cars{
    
    public Maruti(){
        setBrand();
        setModel();
        setPrice();
        setResaleValue();
}

    @Override
    public final void setResaleValue() {
        this.resaleValue=this.price*0.6;
    }
    @Override
    public final void setBrand(){
        this.brand="Maruti";
    }
    @Override
    public int compareTo(Cars car) {
        return this.getModel().compareTo(car.getModel());
    }
    
}
