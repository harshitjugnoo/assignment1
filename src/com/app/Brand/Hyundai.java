package com.app.Brand;

import com.app.entities.Cars;

public class Hyundai extends Cars {

    public Hyundai() {
        setBrand();
        setModel();
        setPrice();
        setResaleValue();
    }

    @Override
    public final void setResaleValue() {
        this.resaleValue = this.price * 0.4;
    }

    @Override
    public final void setBrand() {
        this.brand = "Hyundai";
    }

    @Override
    public int compareTo(Cars car) {
        return this.getModel().compareTo(car.getModel());
    }
}
