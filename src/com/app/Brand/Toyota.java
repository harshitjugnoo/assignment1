/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.Brand;
//import java.util.Scanner;
import com.app.entities.Cars;

/**
 *
 * @author Harshit
 */
public class Toyota extends Cars {

    public Toyota(){
        setBrand();
        setModel();
        setPrice();
        setResaleValue();
    }

    @Override
    public final void setResaleValue() {
        this.resaleValue = this.price * 0.8;
    }
    @Override
    public final void setBrand(){
        this.brand="Toyota";
    }
    @Override
    public int compareTo(Cars car) {
        return this.getModel().compareTo(car.getModel());
    }
}
