/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import com.app.Brand.Hyundai;
import com.app.Brand.Maruti;
import com.app.Brand.Toyota;
import static com.app.entities.AddCustomer.userInput;
import java.util.*;
import com.app.utilities.AppConstants;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Database implements AppConstants {

    static int count = 0;
    static int option;
    static int cid;
    static Scanner userOption = new Scanner(System.in);
    static Map<Integer, AddCustomer> custMap = new HashMap<>();
    static Object mapKeys[];
    static Set<Object> mySet = new HashSet<>();
    static Random random;
    static int luckyDraw1, luckyDraw2, luckyDraw3;
    static SortedSet<Map.Entry<Integer, AddCustomer>> sortedset = new TreeSet<>(
            new Comparator<Map.Entry<Integer, AddCustomer>>() {
        @Override
        public int compare(Map.Entry<Integer, AddCustomer> e1,
                Map.Entry<Integer, AddCustomer> e2) {
            return e1.getValue().name.compareTo(e2.getValue().name);
        }
    });

    //
    public static void main(String[] args) {
        boolean flag = false;
        boolean check;
        do {
            System.out.print(" ***Car Purchase Utility***  \n");
            System.out.print("1. Add a Customer \n");
            System.out.print("2. Add a Car \n");
            System.out.print("3. Display all \n");
            System.out.print("4. Display specific customer \n");
            System.out.print("5. Prize \n");
            System.out.print("6. Exit \n");
            System.out.println("*************************************");
            System.out.print("Please Enter your option \n");
            do {
                if (userOption.hasNextInt()) {
                    option = userOption.nextInt();

                    switch (option) {
                        case ADD_CUSTOMER:
                            addcust();
                            flag = false;
                            break;
                        case ADD_CAR:
                            addCar();
                            flag = false;
                            break;
                        case DISP_ALL:
                            displayAll();
                            flag = false;
                            break;
                        case DISP_CUST:
                            displayCustomer();
                            flag = false;
                            break;
                        case PRIZE:
                            check = toCheck();
                            if(check)
                            getPrizeWinners();
                            flag = false;
                            break;
                        case EXIT:
                            break;
                        default:
                            System.out.print("Please Enter a valid option : ");
                            flag = true;
                            break;
                    }
                } else {
                    System.out.print("Please Enter a valid option : ");
                    //userOption.hasNextLine();
                    flag = true;
                }
                userOption.nextLine();
            } while (flag);
        } while (option != EXIT);

    }

    private static void addCar() {
        boolean flag = false;
        System.out.print("Enter Customer's ID : ");
        if (userOption.hasNextInt()) {
            cid = userOption.nextInt();
            AddCustomer cust = custMap.get(cid);
            System.out.println("Enter the brand : \n ");
            System.out.println("1. Hyundai");
            System.out.println("2. Toyota");
            System.out.println("3. Maruti");
            Cars obj = null;
            do {
                if (userOption.hasNextInt()) {
                    option = userOption.nextInt();

                    switch (option) {
                        case HYUNDAI:
                            obj = new Hyundai();
                            flag = false;
                            break;
                        case TOYOTA:
                            obj = new Toyota();
                            flag = false;
                            break;
                        case MARUTI:
                            obj = new Maruti();
                            flag = false;
                            break;
                        default:
                            System.out.print("Please Enter a valid option : ");
                            flag = true;
                            break;
                    }
                } else {
                    System.out.print("Please Enter a valid option : ");
                    //userOption.hasNextLine();
                    flag = true;
                }
                //userOption.nextLine();
            } while (flag);
            cust.list.add(obj);
            System.out.println("The car has been added successfully!");
        }
    }

    private static void displayCustomer() {
        System.out.println("Enter Customer ID : ");
        if (userInput.hasNextInt()) {
            cid = userInput.nextInt();
        }
        AddCustomer cust = custMap.get(cid);
        cust.display();
    }

    private static void displayAll() {

        sortedset.addAll(custMap.entrySet());
        for (Object o : sortedset) {
            String[] temp = o.toString().split("=");
            int i = Integer.parseInt(temp[0]);
            custMap.get(i).display();


        }
        //System.out.println(sortedset);

        /*for (Map.Entry<Integer, AddCustomer> e : custMap.entrySet()) {
         sortedset.put(e.getValue().id, e.getValue());
         e.getValue().display();
         }*/
    }

    private static void addcust() {
        AddCustomer cust = new AddCustomer();
        custMap.put(cust.id, cust);
        System.out.println("The Customer is successfully added!");
        count++;
    }

    private static void getPrizeWinners() {

        mapKeys = custMap.keySet().toArray();

        for (int i = 0; i < 6; i++) {
            Object key = mapKeys[new Random().nextInt(mapKeys.length)];
            if (mySet.contains(key)) {
                i--;
            }
            mySet.add(key);
            //System.out.println("KeySet values are : " + mySet);
        }

        System.out.println("Enter lucky draw 3 : ");
        luckyDraw3 = userOption.nextInt();
        System.out.println("Enter lucky draw 2 : ");
        luckyDraw2 = userOption.nextInt();
        System.out.println("Enter lucky draw 1 : ");
        luckyDraw1 = userOption.nextInt();
        if (mySet.contains(luckyDraw3)) {
            System.out.print("Customer IDs of Lucky Winners are : " + luckyDraw3);
        }
        if (mySet.contains(luckyDraw2)) {
            System.out.print(", " + luckyDraw2);
        }
        if (mySet.contains(luckyDraw1)) {
            System.out.print(", " + luckyDraw1);
        }
    }

    private static boolean toCheck() {
        if (count < 6) {
            System.out.println("This option cannot be Selected right now. \n Please add at least 6 customers.");
            return false;
        } else {
            return true;
        }

    }
}
