/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import static com.app.main.Main.userOption;

public abstract class Cars implements Comparable<Cars> {

    public String brand;
    String model;
    public double price;
    public double resaleValue;

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public double getResaleValue() {
        return resaleValue;
    }

    public void setModel() {
        System.out.println("Enter the Model : ");
        userOption.nextLine();
        if (userOption.hasNextLine()) {
            this.model = userOption.nextLine();
        }
    }

    public void setPrice() {
        System.out.println("Enter the Price : ");
        if (userOption.hasNextDouble()) {
            this.price = userOption.nextDouble();
        }
    }
    
    public abstract void setBrand();

    public abstract void setResaleValue();

    public void display() {
        System.out.println("Brand : " + getBrand());
        System.out.println("Model : " + getModel());
        System.out.println("Price : " + getPrice());
        System.out.println("Resale Value : " + getResaleValue() + "\n");
    }

    
    
}
