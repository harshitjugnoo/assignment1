/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class AddCustomer {

    public String name;
    public int id;
    public List<Cars> list = new ArrayList<>();
    public static Scanner userInput = new Scanner(System.in);

    public AddCustomer() {
        Boolean flag = true;
        String temp;
        int tmp;
        userInput.nextLine();
        System.out.print("Enter Customer's name : ");
        if (userInput.hasNextLine()) {
            do {
                temp = userInput.nextLine().trim();
                for (int i = 0; i < temp.length(); i++) {
                    char ch = temp.charAt(i);
                    if (ch >= 48 && ch <= 57) {
                        System.out.print("Please enter a valid Customer Name : ");
                        flag = false;
                        break;
                    } else {
                        flag = true;
                    }
                }
            } while (!flag);
            name = temp;
        }
        System.out.print("Enter Customer's ID : ");
        do {
            if (userInput.hasNextInt()) {
                id = userInput.nextInt();
                userInput.nextLine();
                break;
            } else {
                System.out.print("Please Enter a valid Customer ID : ");
                userInput.nextLine();
              }

        } while (flag);

    }

    public void display() {
        System.out.println("Customer ID : " + id);
        System.out.println("Name : " + name);
        Collections.sort(list);
        for (Cars car : list) {
            car.display();
        }
        System.out.println("*************************************");
    }
}
